publish project
> dotnet publish -f netcoreapp3.1 -o publish

build docker image
> docker build . -t swagger:log

docker run with volume binding(aka. File system sharing)
> docker run -p 80:80 -v /Users/paul_cheung/Desktop/docker/aspdotnetcore/v-map-from-docker:/app/logs swagger:log  
**NOTE**: This command will map the host path '**/Users/.../v-map-from-docker**' to directory '**/app/logs**' in the container. Of course, It is neccessary to grant the access permission for host file sharing.