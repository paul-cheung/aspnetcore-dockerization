﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace aspdotnetcore.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        /// <summary>
        /// this api is for getting weathers[this line of description is from xml comment]
        /// </summary>
        /// <remarks>
        /// Sample: WeatherForecast/Get to return list of data.
        /// Returns: <br />
        /// 1.Success: success to get data. <br />
        /// 2.NotFound: data not found.
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(200, "get weathers", typeof(IEnumerable<WeatherForecast>))]
        [ProducesResponseType(typeof(IList<int>), 201)]
        public IEnumerable<WeatherForecast> Get()
        {
            _logger.LogError($">>>>>>>>request received at {DateTime.UtcNow}");
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}
